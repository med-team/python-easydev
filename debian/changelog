python-easydev (0.13.3+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Support poetry-core 2.0 (closes: #1094057).

 -- Colin Watson <cjwatson@debian.org>  Mon, 27 Jan 2025 20:09:55 +0000

python-easydev (0.13.2+dfsg1-3) unstable; urgency=medium

  * Team upload.
  * Remove python3-appdirs from runtime dependencies too.
  * Remove obsolete runtime dep on python3-pkg-resources.

 -- Alexandre Detiste <tchet@debian.org>  Thu, 04 Jul 2024 23:04:35 +0200

python-easydev (0.13.2+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * d/rules: Skip test_url test; requires internet connection.
    (Closes: #1071075)

 -- Sergio Durigan Junior <sergiodj@debian.org>  Tue, 11 Jun 2024 14:08:14 -0400

python-easydev (0.13.2+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * redo tarball
  * do not exclude appdirs.py anymore,
    it is just a thin compatibility wrapper now

 -- Alexandre Detiste <tchet@debian.org>  Sun, 28 Apr 2024 23:01:25 +0200

python-easydev (0.13.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.13.2+dfsg
  * the pypi archive is missing tests/, update d/watch to use Github
  * update dependencies:
    - python3-mock (Closes: #1063816)
    - python3-appdirs
    - python3-setuptools
    + python3-platformdirs
    + python3-poetry
    + python3-poetry-core
  * remove Python2 vs 3 cruft
  * remove old patches

 -- Alexandre Detiste <tchet@debian.org>  Sun, 28 Apr 2024 22:04:35 +0200

python-easydev (0.12.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * d/copyright: Remove superfluous file pattern, easydev/package.py
  * d/patches: Set Forwarded to not-needed for no_pkg_resources.patch and
    privacy.patch
  * d/source/options: Not needed, egg-info cleaned by dh-sequence-python3
  * d/rules: Remove manual cleaning of egg-info and add pybuild_autopkgtest
    conditional for test preparation
  * d/control: Replace autopkgtest-pkg-python with autopkgtest-pkg-pybuild

 -- Lance Lin <lq27267@gmail.com>  Tue, 05 Dec 2023 16:37:57 +0000

python-easydev (0.12.0+dfsg-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Mohammed Bilal ]
  * remove python3-nose from B-D (Closes: #1018484)

  [ Jeroen Ploemen ]
  * Rules: use execute_after instead of an override for dh_install.

 -- Mohammed Bilal <mdbilal@disroot.org>  Sun, 04 Dec 2022 20:32:25 +0000

python-easydev (0.12.0+dfsg-3) unstable; urgency=medium

  * debian/rules
    - fix pytest -k syntax for pytest7; Closes: #1013723

 -- Sandro Tosi <morph@debian.org>  Sun, 26 Jun 2022 13:00:17 -0400

python-easydev (0.12.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Move package to Debian Python Team

 -- Andreas Tille <tille@debian.org>  Sun, 02 Jan 2022 16:55:42 +0100

python-easydev (0.12.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.  (Closes: #997503)
  * Standards-Version: 4.6.0 (routine-update)
  * No tab in license text (routine-update)
  * Refresh external-appdirs.patch
  * Refresh no_pkg_resources.patch
  * d/rules: remove easydev_buildPackage renaming.
    The executable script is not provided anymore.
  * d/copyright: bump copyright year

 -- Étienne Mollier <emollier@debian.org>  Thu, 28 Oct 2021 21:29:43 +0200

python-easydev (0.10.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Advertise repackaged source

 -- Andreas Tille <tille@debian.org>  Tue, 29 Sep 2020 09:58:41 +0200

python-easydev (0.9.38-2) unstable; urgency=medium

  * Source only upload (Closes: #943912).

 -- Olivier Sallou <osallou@debian.org>  Mon, 04 Nov 2019 10:22:25 +0000

python-easydev (0.9.38-1) unstable; urgency=medium

  * New upstream release (Closes: #937720).

 -- Olivier Sallou <osallou@debian.org>  Mon, 28 Oct 2019 15:56:52 +0000

python-easydev (0.9.37-2) UNRELEASED; urgency=medium

  * Remove python2 package  (Closes: #937720).

 -- Olivier Sallou <osallou@debian.org>  Mon, 28 Oct 2019 15:04:58 +0000

python-easydev (0.9.37-1) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Fix watch file
  * debhelper 12
  * Activate Testsuite field
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright

  [ Piotr Ożarowski ]
  * do not use pkg_resources

  [ Nick Morrott ]
  * d/control: refresh build-dependencies for tests
  * d/patches: refresh external-appdirs.patch
  * d/rules: generate egg_info for tests
  * d/rules: skip test requiring network access
  * d/rules: delete coverage report from build tree
  * Drop unused python-easydev.lintian-overrides.

 -- Andreas Tille <tille@debian.org>  Wed, 23 Jan 2019 07:45:34 +0100

python-easydev (0.9.35+dfsg-2) unstable; urgency=medium

  * Correct ITP bug number in previous changelog entry
  * Add dependencies on python-pkg-resources (Closes: #896292, #896402)
  * Bump Standards-Version to 4.1.3

 -- Afif Elghraoui <afif@debian.org>  Sun, 22 Apr 2018 17:53:49 -0400

python-easydev (0.9.35+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #878964)

 -- Afif Elghraoui <afif@debian.org>  Wed, 18 Oct 2017 01:56:51 -0400
